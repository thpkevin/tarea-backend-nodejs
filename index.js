// import express from 'express'; 
const express = require('express');
const { BASE_URL, BASE_URL_V2, personasPATH } = require('./routes');

const PORT = 3000;
const personas = [
	{
		_id: 1,
		nombre: 'Ismael Alexander Malca Castro',
		edad: 23,
		job: 'SE Senior FullStack MERN - MEAN developer'
	},
	{
		_id: 2,
		nombre: 'Pepe Gonzales',
		edad: 24,
		job: 'Administrador de Sistemas'
	},
	{
		_id: 3,
		nombre: 'Cesar ',
		edad: 25,
		job: 'CEO-CORP'
	},
]; 

const personas2 = [
	{
		_id: 1,
		nombre: 'Ismael Alexander Malca Castro',
		edad: 23,
		job: 'SE Senior FullStack MERN - MEAN developer',
		isSingle: false,
	},
	{
		_id: 2,
		nombre: 'Pepe Gonzales',
		edad: 24,
		job: 'Administrador de Sistemas',
		isSingle: false,
	},
	{
		_id: 3,
		nombre: 'Cesar ',
		edad: 25,
		job: 'CEO-CORP',
		isSingle: true
	},
]; 

const app = express();

app.use(express.json());

app.get('/', (request, response) => {
	
	return response.sendFile(__dirname.concat('/index.html'), err => {
		if(err){
			console.error(err);
		}
	});
});

app.get('/testjson', (request, response) => {
	const body = {
		saludo: "hola estoy en mi primera ruta con NodeJS y Express JS"
	};
	return response.send(body);
});

app.get(`/api/personas`, (_, response) => {
	return response.send(personas);
});

app.post(`${BASE_URL}${personasPATH}`, (request, response) => {
	try {
		const { nombre, edad, job } = request.body;
		if(nombre && edad && job ){
	
			const nuevaPersona = {
				_id: personas.length + 1,
				nombre,
				edad,
				job
			}
			personas.push(nuevaPersona)

			return response.status(201).send({message: 'Persona agregada correctamente '})
		}
		
		return response.status(403).send({
			message: 'Datos Incorrectos',
			nombre,
			edad,
			job
		});

	} catch (e) {
		console.error(e);
		response.status(500).send(
			{error: JSON.stringify(e) }
		);
		return;
	}
});

app.patch(`${BASE_URL}${personasPATH}`, (request, response) => {
	try {
		const persona = request.body;
		if(persona._id ){

			const personaIndex = personas.findIndex(value => value._id === persona._id );

			if(personaIndex > -1 ){
				// CASO DE PROPIEDAD ESPECIFICA
				// const persona = request.body;
				// personas[personaIndex] = { ...personas[personaIndex], nombre }

				personas[personaIndex] = {...persona, _id: personas[personaIndex]._id };
				return response.status(201).send({message: 'Persona actualizada correctamente '});
			}

			return response.status(400).send({message: 'No se han encontrado coincidencias'});
			
		}
		return response.status(403).send({message: 'Faltan campos'});
	} catch (error) {
		return response.status(500).send({message: 'Ha ocurrido un error en el servidor'});
	}
});

app.get(`${BASE_URL_V2}${personasPATH}`, (_, response) => {
	return response.send(personas2);
});

// TODO - REALIZAR EL DELETE
// nota: splice 

app.delete(`${BASE_URL}${personasPATH}`, (request, response) => {
	try {
		const persona = request.body;
		if(persona._id ){
			const personaIndex = personas.findIndex(value => value._id === persona._id );
	
			if(personaIndex > -1 ){
				personas.splice(personaIndex, 1);
				return response.status(201).send({message: 'Persona eliminada correctamente '});
			}

			return response.status(400).send({message: 'No se han encontrado coincidencias'});
			
		}
		return response.status(403).send({message: 'Faltan campos'});
	} catch (error) {
		return response.status(500).send({message: 'Ha ocurrido un error en el servidor'});
	}
});


app.listen(PORT, () => {
	console.log(`the application is running on http://localhost:${PORT}`);
});
